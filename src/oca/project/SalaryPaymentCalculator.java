package oca.project;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

//class that calculates salaries for contractor/salaried employees including 
//salary increases and bonuses for salaried employees
public class SalaryPaymentCalculator {

    //delcaring variables for the class
    private static final int MONTHS_IN_YEAR = 12;
    private static final int FORTNIGHTS_IN_YEAR = 26;
    
    //setting up the decimal format
    static DecimalFormat df = new DecimalFormat("00.00");
    
    //static method to calculate year difference between two parameters passed to it,
    //and then the method will be used to calculate salary increase
    public static double yearsBetween(Date startDate, Date currentDate){ 
        double numOfYears = (int)( (currentDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24 * 365) );
        return numOfYears;
    }
    
    //static method to calculate current salary of person with increase
    public static double calculateCurrentSalaryWithIncrease(double baseSalary, Date dateStarted){
        Date currentDate = new GregorianCalendar(2015, Calendar.JANUARY, 01).getTime();       
        int yearsWorked = (int)yearsBetween(dateStarted, currentDate);
        int percentRaise = 0;
        //if 2 or more years have been worked, then for each two years percentRaise increases by 2%
        if (yearsWorked > 1){
            for (int i = 0; i <= yearsWorked; i+=2){
            percentRaise += 2;
            }
            //baseSalary recalculated taking into account the percentRaise
            baseSalary = ((percentRaise / 100) * baseSalary) + baseSalary;
        }
        return Double.parseDouble(df.format(baseSalary));        
    }
    
    //static method to calculate pay with increase for salaried workers
    public static String calculateFinalYearlyPay(ISalariedPerson salariedPerson){
        double finalYearlyPay = calculateCurrentSalaryWithIncrease(salariedPerson.getBaseSalary(),
                salariedPerson.getStartDate()) + salariedPerson.getCurrentBonus();
        return df.format(finalYearlyPay);
    }
    
    //static method to calculate pay for the monthly or fortnightly payment due for a Person
    public static double calculatePay(ISalariedPerson salariedPerson){
        double totalPayForPeriod = 0;
        //calculates current pay due, based on whether salariedPerson is payed monthly or fortnightly
        if (salariedPerson.getTimePeriod().equals(TimePeriod.MONTHLY)){
            double finalYearlyPay = Double.parseDouble(calculateFinalYearlyPay(salariedPerson));
            totalPayForPeriod = finalYearlyPay / MONTHS_IN_YEAR;
        }
        else if (salariedPerson.getTimePeriod().equals(TimePeriod.FORTNIGHTLY)){
            double finalYearlyPay = Double.parseDouble(calculateFinalYearlyPay(salariedPerson));
            totalPayForPeriod = finalYearlyPay / FORTNIGHTS_IN_YEAR;
        }
        return Double.parseDouble(df.format(totalPayForPeriod));
    }
    
}
