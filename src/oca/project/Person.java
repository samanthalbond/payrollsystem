package oca.project;

import java.text.SimpleDateFormat;
import java.util.Date;

//class of which all staff member objects belong
public class Person {
    
    //setting format for date objects
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyy");
    
    //declaring instance variables
    private String firstName;
    private String lastName;
    private Date dateOfBirth;

    
    //overridden toString method to display "Person" name variables in 
    //comboboxes (PersonalDataForm & Report Form)
    @Override
    public String toString(){
        return this.firstName + " " + this.lastName;
    }
    
    //getters and setters for instance variables
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
