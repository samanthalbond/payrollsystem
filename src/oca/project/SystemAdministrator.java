package oca.project;
/**
 * Class used to define SystemAdministrator objects 
 */
public class SystemAdministrator extends SalariedSubordinate {

    public SystemAdministrator(IManager manager) {
        super(65_000.00, manager);
    }

}
