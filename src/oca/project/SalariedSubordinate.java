package oca.project;

import java.util.Date;

//class that contains the variables and methods for salaried workers
public class SalariedSubordinate extends Person implements ISalariedPerson, ISubordinate {

    //declaring instance variables
    private Date startDate;
    private IManager manager;
    private TimePeriod timePeriod;
    private double baseSalary;
    private double currentBonus;
    

    //declaring constructor
    public SalariedSubordinate(double baseSalary, IManager manager){
        this.baseSalary = baseSalary;
        this.manager = manager;
    }
    
    
    //getter and setter methods for the class
    @Override
    public TimePeriod getTimePeriod() {
        return timePeriod;
    }
    
    @Override
    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }

    @Override
    public double getBaseSalary() {
        return baseSalary;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }
    
    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    @Override
    public double getCurrentBonus() {
        return currentBonus;
    }

    @Override
    public void setCurrentBonus(double bonus) {
        this.currentBonus = bonus;
    }

    @Override //maybe done wrong?
    public double calculatePay() throws Exception {
        return SalaryPaymentCalculator.calculatePay(this);
    }

    @Override
    public IManager getManager() {
        return manager;
    }

}
