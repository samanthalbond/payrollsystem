package oca.project;

//Interface to be implemented by all people working at the company who have a manager
public interface ISubordinate {
    
    //getter method for "manager"
    IManager getManager();
}
