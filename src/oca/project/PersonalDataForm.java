package oca.project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
/**
 *
 * Form that displays personal details and pays contracted employees
 */
public class PersonalDataForm extends javax.swing.JFrame {

    //setting up date format
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    //creating new list to hold the report items
    private ArrayList<PayrollReportItem> listOfReportItems = new ArrayList<>();
    //encapsulating listOfReportItems
    public ArrayList<PayrollReportItem> getListOfReportItems() {
        return listOfReportItems;
    }
    public void setListOfReportItems(ArrayList<PayrollReportItem> listOfReportItems) {
        this.listOfReportItems = listOfReportItems;
    }

    
    /**
     * Creates new form DataEntryForm
     * @param listOfReportItems
     */
    public PersonalDataForm(ArrayList<PayrollReportItem> listOfReportItems) {
        initComponents();
        this.getContentPane().setBackground(new java.awt.Color(231,231,231));
        panContractor.setBackground(new java.awt.Color(231,231,231));
        panSalariedPerson.setBackground(new java.awt.Color(231,231,231));
    }
    
    //declaring ArrayList to store "Person" type objects
    private ArrayList<Person> listOfAllPersons; // = new ArrayList<>();
    //encapsulating ArrayList of "Person" objects
    public ArrayList<Person> getListOfAllPersons(){
        return listOfAllPersons;
    }
    public void setListOfAllPersons(ArrayList<Person> listOfAllPersons) {
        this.listOfAllPersons = listOfAllPersons;
    }
    
    //declaring and encapulating MainForm which will be used for navigation between forms
    private MainForm mainForm ; 
    
    public MainForm getMainForm() {
        return mainForm;
    }
    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }
    
    
    // declaring DefaultComboBox model for cboName
    DefaultComboBoxModel nameModel;
    
    //creates the model to populate cboName with the list of people working in the company
    public void populateNameComboBox(ArrayList<Person> listOfAllPersons){
        nameModel = new DefaultComboBoxModel();
        for (Person p: listOfAllPersons){
            nameModel.addElement(p);
        }
        cboName.setModel(nameModel);
    }
    
    //method that displays personal details based on the selection made in cboName
    public void getPersonalData() {
        try{
            //retrieves Person object from user selection and checks to see if its
            //and instance of SalariedSubordinate or of ContractSubordinate
            Person selectedPerson = (Person) cboName.getSelectedItem();
            if (selectedPerson instanceof SalariedSubordinate){
                //casts object to SalariedSubordinate so its methods can be called
                //then sets textboxes with data
                SalariedSubordinate selectedSubordinate = (SalariedSubordinate) selectedPerson;
                txtDOB.setText(sdf.format(selectedSubordinate.getDateOfBirth()));
                txtDOB.setEditable(false);
                txtStartDate.setText(sdf.format(selectedSubordinate.getStartDate()));
                txtStartDate.setEditable(false);
                lstContractor.setSelectedIndex(1);
                lstContractor.setEnabled(false);
                if (selectedSubordinate.getTimePeriod() == TimePeriod.FORTNIGHTLY){
                    cboTimePeriod.setSelectedIndex(1);
                }    
                else if (selectedSubordinate.getTimePeriod() == TimePeriod.MONTHLY){
                    cboTimePeriod.setSelectedIndex(2);
                }
                cboTimePeriod.setEditable(false);
                txtBaseSalary.setText(SalaryPaymentCalculator.df.format(selectedSubordinate.getBaseSalary()));
                txtBaseSalary.setEditable(false);
            }
            else if (selectedPerson instanceof ContractSubordinate){
                //casts object to ContractSubordinate so its methods can be called
                //then sets textboxes with data
                ContractSubordinate selectedSubordinate = (ContractSubordinate) selectedPerson;
                txtDOB.setText(sdf.format(selectedSubordinate.getDateOfBirth()));
                txtDOB.setEditable(false);
                lstContractor.setSelectedIndex(0);
                lstContractor.setEnabled(false);
                cboTimePeriod.setSelectedIndex(2);
                cboTimePeriod.setEditable(false);
                txtHourlyRate.setText(SalaryPaymentCalculator.df.format(selectedSubordinate.getHourlyRate()));
                txtHourlyRate.setEditable(false);
            }
        }
        catch (NumberFormatException nfe){
            nfe.toString();
        }
        catch (Exception e){
            e.toString();
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        msgMessage = new javax.swing.JOptionPane();
        button1 = new java.awt.Button();
        lblName = new javax.swing.JLabel();
        cboName = new javax.swing.JComboBox();
        lblDOB = new javax.swing.JLabel();
        txtDOB = new javax.swing.JTextField();
        lblStartDate = new javax.swing.JLabel();
        txtStartDate = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstContractor = new javax.swing.JList();
        panContractor = new javax.swing.JPanel();
        lblContractor = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtStartOfPayPeriod = new javax.swing.JTextField();
        txtHoursOfWork = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtHourlyRate = new javax.swing.JTextField();
        btnAddContract = new javax.swing.JButton();
        panSalariedPerson = new javax.swing.JPanel();
        txtBaseSalary = new javax.swing.JTextField();
        lblSalaried = new javax.swing.JLabel();
        lblBaseSalary1 = new javax.swing.JLabel();
        cboTimePeriod = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();

        button1.setLabel("button1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Best IT - Personal Information");

        lblName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblName.setText("Name:");

        cboName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cboName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboNameItemStateChanged(evt);
            }
        });

        lblDOB.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDOB.setText("Date of Birth:");

        txtDOB.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtDOB.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtDOB.setEnabled(false);

        lblStartDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblStartDate.setText("Start Date:");

        txtStartDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtStartDate.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtStartDate.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Contractor:");

        lstContractor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lstContractor.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Yes", "No" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstContractor.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstContractor.setEnabled(false);
        lstContractor.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstContractorValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstContractor);

        panContractor.setForeground(new java.awt.Color(153, 153, 153));
        panContractor.setPreferredSize(new java.awt.Dimension(327, 202));

        lblContractor.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblContractor.setText("Contractor Worker");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Start date of pay period:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Hours of Work:");

        txtStartOfPayPeriod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtHoursOfWork.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Hourly rate: ");

        txtHourlyRate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtHourlyRate.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtHourlyRate.setEnabled(false);

        btnAddContract.setBackground(new java.awt.Color(216, 244, 255));
        btnAddContract.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnAddContract.setText("Add");
        btnAddContract.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddContractActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panContractorLayout = new javax.swing.GroupLayout(panContractor);
        panContractor.setLayout(panContractorLayout);
        panContractorLayout.setHorizontalGroup(
            panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panContractorLayout.createSequentialGroup()
                .addComponent(lblContractor)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panContractorLayout.createSequentialGroup()
                .addGroup(panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(panContractorLayout.createSequentialGroup()
                        .addGroup(panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel2))
                        .addGap(28, 28, 28)
                        .addGroup(panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtStartOfPayPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtHoursOfWork, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtHourlyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panContractorLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnAddContract, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panContractorLayout.setVerticalGroup(
            panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panContractorLayout.createSequentialGroup()
                .addComponent(lblContractor)
                .addGap(18, 18, 18)
                .addGroup(panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panContractorLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(76, 76, 76))
                    .addGroup(panContractorLayout.createSequentialGroup()
                        .addComponent(txtStartOfPayPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(panContractorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtHoursOfWork, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addComponent(txtHourlyRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAddContract, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        panSalariedPerson.setForeground(new java.awt.Color(153, 153, 153));

        txtBaseSalary.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtBaseSalary.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        txtBaseSalary.setEnabled(false);

        lblSalaried.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblSalaried.setText("Salaried Worker");

        lblBaseSalary1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblBaseSalary1.setText("Base Salary: ");

        javax.swing.GroupLayout panSalariedPersonLayout = new javax.swing.GroupLayout(panSalariedPerson);
        panSalariedPerson.setLayout(panSalariedPersonLayout);
        panSalariedPersonLayout.setHorizontalGroup(
            panSalariedPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panSalariedPersonLayout.createSequentialGroup()
                .addComponent(lblSalaried)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panSalariedPersonLayout.createSequentialGroup()
                .addComponent(lblBaseSalary1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(txtBaseSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panSalariedPersonLayout.setVerticalGroup(
            panSalariedPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panSalariedPersonLayout.createSequentialGroup()
                .addComponent(lblSalaried)
                .addGap(18, 18, 18)
                .addGroup(panSalariedPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBaseSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBaseSalary1))
                .addContainerGap(148, Short.MAX_VALUE))
        );

        cboTimePeriod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cboTimePeriod.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "WEEKLY", "FORTNIGHTLY", "MONTHLY" }));
        cboTimePeriod.setEnabled(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Time Period:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText(" Personal Information");

        btnBack.setBackground(new java.awt.Color(216, 244, 255));
        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panSalariedPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panContractor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTimePeriod, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblDOB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel6)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDOB)
                    .addComponent(txtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStartDate)
                    .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cboTimePeriod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panContractor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panSalariedPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //when user selects the name, the personal data is updated
    private void cboNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboNameItemStateChanged
        getPersonalData();
    }//GEN-LAST:event_cboNameItemStateChanged

    //displays only contractor or salaried worker information based on the selection made in the contractor list
    private void lstContractorValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstContractorValueChanged
        try {
            if (lstContractor.isSelectedIndex(0)) {
                panContractor.setVisible(true);
                panSalariedPerson.setVisible(false);
            }
            else {
                panSalariedPerson.setVisible(true);
                panContractor.setVisible(false);
            }
        }
        catch (Exception e){
            e.toString();
        }
    }//GEN-LAST:event_lstContractorValueChanged

    //event handler to assign salary to contractor
    private void btnAddContractActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddContractActionPerformed
        try {
            if (cboName.getSelectedItem() instanceof ContractSubordinate) {
                //checks if hours of work have been entered, if not then displays message to user
                if (txtHoursOfWork.getText().isEmpty()){
                    msgMessage.showMessageDialog(this, "Please specify the number of hours worked", "Annoucement", JOptionPane.INFORMATION_MESSAGE);
                }
                //checks if start of pay period has been entered, if not then displays message to user
                else if (txtStartOfPayPeriod.getText().isEmpty()) {
                    msgMessage.showMessageDialog(this, "Please enter the start date of the pay period", "Annoucement", JOptionPane.INFORMATION_MESSAGE);
                }
                //if details have been entered in the textboxes then calls ContractSubordinates methods to
                //calculate the pay, then creates a new PayrollReportItem and passes it to the ArrayLists in class and in MainForm
                //then confirms payment in message to user
                else if (txtHoursOfWork.getText() != "" && txtStartOfPayPeriod.getText() != ""){
                    ContractSubordinate selectedSubordinate = (ContractSubordinate) cboName.getSelectedItem();
                    selectedSubordinate.setHourlyRate(Double.parseDouble(txtHourlyRate.getText()));
                    Date startDatePayPeriod = sdf.parse(txtStartOfPayPeriod.getText());
                    double hoursWorked = Double.parseDouble(txtHoursOfWork.getText());
                    double totalPay = selectedSubordinate.calculatePay(startDatePayPeriod, hoursWorked);
                    PayrollReportItem reportItem = new PayrollReportItem((Person) selectedSubordinate, totalPay, startDatePayPeriod, TimePeriod.MONTHLY);
                    getListOfReportItems().add(reportItem);
                    mainForm = new MainForm(getListOfAllPersons());
                    mainForm.setListOfPayrollReportItems(getListOfReportItems());
                    msgMessage.showMessageDialog(this, "Contract payment of $" + totalPay + " was added", "Announement", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
        catch (ParseException pe) {
            pe.toString();
        }
        catch (Exception e) {
            e.toString();
        }
    }//GEN-LAST:event_btnAddContractActionPerformed
    //event handler for the Back button that closes the current form and displays the main form 
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.dispose();
        try {
            mainForm = new MainForm(getListOfAllPersons());
        } catch (ParseException pe) {
            pe.toString();
        }
        getMainForm().setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PersonalDataForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PersonalDataForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PersonalDataForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PersonalDataForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new PersonalDataForm().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddContract;
    private javax.swing.JButton btnBack;
    private java.awt.Button button1;
    private javax.swing.JComboBox cboName;
    private javax.swing.JComboBox cboTimePeriod;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBaseSalary1;
    private javax.swing.JLabel lblContractor;
    private javax.swing.JLabel lblDOB;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblSalaried;
    private javax.swing.JLabel lblStartDate;
    private javax.swing.JList lstContractor;
    private javax.swing.JOptionPane msgMessage;
    private javax.swing.JPanel panContractor;
    private javax.swing.JPanel panSalariedPerson;
    private javax.swing.JTextField txtBaseSalary;
    private javax.swing.JTextField txtDOB;
    private javax.swing.JTextField txtHourlyRate;
    private javax.swing.JTextField txtHoursOfWork;
    private javax.swing.JTextField txtStartDate;
    private javax.swing.JTextField txtStartOfPayPeriod;
    // End of variables declaration//GEN-END:variables

}
