package oca.project;

import java.util.ArrayList;
/**
 * Class used to define Manager objects 
 */
public class Manager extends SalariedSubordinate implements IManager {

    //declaring instance variables
    private  ArrayList<ISubordinate> subordinateList = new ArrayList<>();
    private double managerBonusFund = 5000;
    
    //declaring class constructors
    public Manager (double baseSalary, IManager manager, ArrayList<ISubordinate> subList){
        super(baseSalary, manager);
        this.subordinateList = subList;
    }
    public Manager (double baseSalary, IManager manager){
        super(baseSalary, manager);
        subordinateList = new ArrayList<>();
    }
    
    //getter and setter methods for the instance varibles
    @Override
    public ArrayList<ISubordinate> getSubordinateList() {
        return subordinateList;
    }

    @Override
    public double getTotalManagerBonusFund() {
        return managerBonusFund;
    }

    @Override
    public void setTotalManagerBonusFund(double totalManagerBonusFund) {
        this.managerBonusFund = totalManagerBonusFund;
    }

    @Override
    public void applyBonus(double bonus) {
        managerBonusFund = (managerBonusFund - bonus);
    }

    @Override
    public String assignBonus(ISubordinate subordinate, double bonus) {
        return BonusUpdater.assignBonus(this, subordinate, bonus);
    }
    
    @Override
    public double calculatePay() throws Exception {
        return SalaryPaymentCalculator.calculatePay(this);
    }

}
