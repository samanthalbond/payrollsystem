package oca.project;
/**
 * Class used to define SeniorProgramDeveloper objects 
 */
public class SeniorProgramDeveloper extends Manager {

    public SeniorProgramDeveloper(IManager manager) {
        super(75_000.00, manager);
    }

}
