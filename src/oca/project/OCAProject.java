package oca.project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Main class that creates all employees and specifies their managers
 */
public class OCAProject {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {

        //declaring ArrayList to store "Person" type objects
        ArrayList<Person> listOfAllPersons = new ArrayList<>();
        //declaring new date format to pass DoB
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          
        //creating objects for all staff in company
        CEO JamesMint = new CEO();
        JamesMint.setFirstName("James");
        JamesMint.setLastName("Mint");
        JamesMint.setDateOfBirth(df.parse("28/12/1977"));
        JamesMint.setStartDate(df.parse("10/01/2010"));
        JamesMint.setTimePeriod(TimePeriod.MONTHLY);
        AdministrationManager BillJohns = new AdministrationManager(JamesMint);
        BillJohns.setFirstName("Bill");
        BillJohns.setLastName("Johns");
        BillJohns.setDateOfBirth(df.parse("13/04/1981"));
        BillJohns.setStartDate(df.parse("10/05/2012"));
        BillJohns.setTimePeriod(TimePeriod.MONTHLY);
        BranchManager KateHudson = new BranchManager(BillJohns);
        KateHudson.setFirstName("Kate");
        KateHudson.setLastName("Hudson");
        KateHudson.setDateOfBirth(df.parse("13/07/81"));
        KateHudson.setStartDate(df.parse("11/03/2013"));
        KateHudson.setTimePeriod(TimePeriod.MONTHLY);
        BranchManager ScarletGreen = new BranchManager(BillJohns);
        ScarletGreen.setFirstName("Scarlet");
        ScarletGreen.setLastName("Green");
        ScarletGreen.setDateOfBirth(df.parse("20/03/1980"));
        ScarletGreen.setStartDate(df.parse("10/05/2013"));
        ScarletGreen.setTimePeriod(TimePeriod.MONTHLY);
        BranchManager AmandaGreen = new BranchManager(BillJohns);
        AmandaGreen.setFirstName("Amanda");
        AmandaGreen.setLastName("Green");
        AmandaGreen.setDateOfBirth(df.parse("04/04/1979"));
        AmandaGreen.setStartDate(df.parse("14/10/2014"));
        AmandaGreen.setTimePeriod(TimePeriod.MONTHLY);
        FinancialAdministrator EricWhite = new FinancialAdministrator(JamesMint);
        EricWhite.setFirstName("Eric");
        EricWhite.setLastName("White");
        EricWhite.setDateOfBirth(df.parse("21/05/1984"));
        EricWhite.setStartDate(df.parse("10/04/2011"));
        EricWhite.setTimePeriod(TimePeriod.FORTNIGHTLY);
        HROfficer JuliaSmith = new HROfficer(JamesMint);
        JuliaSmith.setFirstName("Julia");
        JuliaSmith.setLastName("Smith");
        JuliaSmith.setDateOfBirth(df.parse("17/02/1973"));
        JuliaSmith.setStartDate(df.parse("13/06/2010"));
        JuliaSmith.setTimePeriod(TimePeriod.MONTHLY);
        ProjectManager VictoriaBell = new ProjectManager(AmandaGreen);
        VictoriaBell.setFirstName("Victoria");
        VictoriaBell.setLastName("Bell");
        VictoriaBell.setDateOfBirth(df.parse("11/01/1978"));
        VictoriaBell.setStartDate(df.parse("30/04/2013"));
        VictoriaBell.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SeniorProgramDeveloper JohnNorton = new SeniorProgramDeveloper(VictoriaBell);
        JohnNorton.setFirstName("John");
        JohnNorton.setLastName("Norton");
        JohnNorton.setDateOfBirth(df.parse("17/07/1980"));
        JohnNorton.setStartDate(df.parse("10/09/2010"));
        JohnNorton.setTimePeriod(TimePeriod.MONTHLY);
        ProgramDeveloper AlexDillon = new ProgramDeveloper(JohnNorton);
        AlexDillon.setFirstName("Alex");
        AlexDillon.setLastName("Dillon");
        AlexDillon.setDateOfBirth(df.parse("10/03/1989"));
        AlexDillon.setStartDate(df.parse("14/09/2014"));
        AlexDillon.setTimePeriod(TimePeriod.FORTNIGHTLY);
        ProjectManager CharlotteHond = new ProjectManager(KateHudson);
        CharlotteHond.setFirstName("Charlotte");
        CharlotteHond.setLastName("Hond");
        CharlotteHond.setDateOfBirth(df.parse("01/07/1988"));
        CharlotteHond.setStartDate(df.parse("28/02/2012"));
        CharlotteHond.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SeniorProgramDeveloper AshleySimpson = new SeniorProgramDeveloper(CharlotteHond);
        AshleySimpson.setFirstName("Ashley");
        AshleySimpson.setLastName("Simpson");
        AshleySimpson.setDateOfBirth(df.parse("12/05/1984"));
        AshleySimpson.setStartDate(df.parse("15/06/2010"));
        AshleySimpson.setTimePeriod(TimePeriod.MONTHLY);
        ProgramDeveloper AndrewSampson = new ProgramDeveloper(AshleySimpson);
        AndrewSampson.setFirstName("Andrew");
        AndrewSampson.setLastName("Sampson");
        AndrewSampson.setDateOfBirth(df.parse("09/04/1984"));
        AndrewSampson.setStartDate(df.parse("04/07/2010"));
        AndrewSampson.setTimePeriod(TimePeriod.FORTNIGHTLY);
        ProjectManager ChristineBennet = new ProjectManager(ScarletGreen);
        ChristineBennet.setFirstName("Christine");
        ChristineBennet.setLastName("Bennet");
        ChristineBennet.setDateOfBirth(df.parse("16/06/1982"));
        ChristineBennet.setStartDate(df.parse("11/06/2011"));
        ChristineBennet.setTimePeriod(TimePeriod.FORTNIGHTLY);
        ProgramDeveloper DavidGalloway = new ProgramDeveloper(ChristineBennet);
        DavidGalloway.setFirstName("David");
        DavidGalloway.setLastName("Galloway");
        DavidGalloway.setDateOfBirth(df.parse("06/12/1981"));
        DavidGalloway.setStartDate(df.parse("05/11/2013"));
        DavidGalloway.setTimePeriod(TimePeriod.MONTHLY);
        Receptionist PeterGordon = new Receptionist(JuliaSmith);
        PeterGordon.setFirstName("Peter");
        PeterGordon.setLastName("Gordon");
        PeterGordon.setDateOfBirth(df.parse("30/09/1983"));
        PeterGordon.setStartDate(df.parse("10/05/2010"));
        PeterGordon.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SalesManager MichaelBrown = new SalesManager(JamesMint);
        MichaelBrown.setFirstName("Michael");
        MichaelBrown.setLastName("Brown");
        MichaelBrown.setDateOfBirth(df.parse("25/12/1985"));
        MichaelBrown.setStartDate(df.parse("21/07/2012"));
        MichaelBrown.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SalesConsultant HelenChan = new SalesConsultant(MichaelBrown);
        HelenChan.setFirstName("Helen");
        HelenChan.setLastName("Chan");
        HelenChan.setDateOfBirth(df.parse("11/01/1991"));
        SystemAdministrator AndrewBlack = new SystemAdministrator(AmandaGreen);
        AndrewBlack.setFirstName("Andrew");
        AndrewBlack.setLastName("Black");
        AndrewBlack.setDateOfBirth(df.parse("15/04/1976"));
        AndrewBlack.setStartDate(df.parse("04/05/2012"));
        AndrewBlack.setTimePeriod(TimePeriod.MONTHLY);
        SystemAdministrator JoanneDowman = new SystemAdministrator(KateHudson);
        JoanneDowman.setFirstName("Joanne");
        JoanneDowman.setLastName("Dowman");
        JoanneDowman.setDateOfBirth(df.parse("13/12/1980"));
        JoanneDowman.setStartDate(df.parse("07/07/2014"));
        JoanneDowman.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SystemAdministrator MichaelPerry = new SystemAdministrator(ScarletGreen);
        MichaelPerry.setFirstName("Michael");
        MichaelPerry.setLastName("Perry");
        MichaelPerry.setDateOfBirth(df.parse("21/11/1977"));
        MichaelPerry.setStartDate(df.parse("01/03/2014"));
        MichaelPerry.setTimePeriod(TimePeriod.FORTNIGHTLY);
        SystemAnalyst AndrewBlank = new SystemAnalyst(VictoriaBell);
        AndrewBlank.setFirstName("Andrew");
        AndrewBlank.setLastName("Blank");
        AndrewBlank.setDateOfBirth(df.parse("15/09/1981"));
        AndrewBlank.setStartDate(df.parse("19/08/2014"));
        AndrewBlank.setTimePeriod(TimePeriod.MONTHLY);
        SystemAnalyst CarolWhite = new SystemAnalyst(CharlotteHond);
        CarolWhite.setFirstName("Carol");
        CarolWhite.setLastName("White");
        CarolWhite.setDateOfBirth(df.parse("20/10/1975"));
        CarolWhite.setStartDate(df.parse("10/01/2014"));
        CarolWhite.setTimePeriod(TimePeriod.MONTHLY);
        SystemAnalyst BradTurner = new SystemAnalyst(ChristineBennet);
        BradTurner.setFirstName("Brad");
        BradTurner.setLastName("Turner");
        BradTurner.setDateOfBirth(df.parse("22/11/1988"));
        BradTurner.setStartDate(df.parse("26/05/2014"));
        BradTurner.setTimePeriod(TimePeriod.MONTHLY);
        Tester JamesCarol = new Tester(VictoriaBell);
        JamesCarol.setFirstName("James");
        JamesCarol.setLastName("Carol");
        JamesCarol.setDateOfBirth(df.parse("24/03/1978"));
        Tester ElenaTang = new Tester(CharlotteHond);
        ElenaTang.setFirstName("Elena");
        ElenaTang.setLastName("Tang");
        ElenaTang.setDateOfBirth(df.parse("11/02/1969"));
        Tester TessaGreen = new Tester(CharlotteHond);
        TessaGreen.setFirstName("Tessa");
        TessaGreen.setLastName("Green");
        TessaGreen.setDateOfBirth(df.parse("31/10/1985"));
        Tester DavidChand = new Tester(ChristineBennet);
        DavidChand.setFirstName("David");
        DavidChand.setLastName("Chand");
        DavidChand.setDateOfBirth(df.parse("11/04/1973"));
        
        //adding staff to the listOfAllPersons arraylist
        listOfAllPersons.add(BillJohns);
        listOfAllPersons.add(KateHudson);
        listOfAllPersons.add(ScarletGreen);
        listOfAllPersons.add(AmandaGreen);
        listOfAllPersons.add(JamesMint);
        listOfAllPersons.add(EricWhite);
        listOfAllPersons.add(JuliaSmith);
        listOfAllPersons.add(AlexDillon);
        listOfAllPersons.add(AndrewSampson);
        listOfAllPersons.add(DavidGalloway);
        listOfAllPersons.add(VictoriaBell);
        listOfAllPersons.add(CharlotteHond);
        listOfAllPersons.add(ChristineBennet);
        listOfAllPersons.add(PeterGordon);
        listOfAllPersons.add(HelenChan);
        listOfAllPersons.add(MichaelBrown);
        listOfAllPersons.add(JohnNorton);
        listOfAllPersons.add(AshleySimpson);
        listOfAllPersons.add(AndrewBlack);
        listOfAllPersons.add(JoanneDowman);
        listOfAllPersons.add(MichaelPerry);
        listOfAllPersons.add(AndrewBlank);
        listOfAllPersons.add(CarolWhite);
        listOfAllPersons.add(BradTurner);
        listOfAllPersons.add(JamesCarol);
        listOfAllPersons.add(ElenaTang);
        listOfAllPersons.add(TessaGreen);
        listOfAllPersons.add(DavidChand);
        
        //adding subordinates to each managers arraylist of subordinates
        JamesMint.getSubordinateList().add(BillJohns);
        JamesMint.getSubordinateList().add(EricWhite);
        JamesMint.getSubordinateList().add(JuliaSmith);
        JamesMint.getSubordinateList().add(MichaelBrown);
        BillJohns.getSubordinateList().add(KateHudson);
        BillJohns.getSubordinateList().add(ScarletGreen);
        BillJohns.getSubordinateList().add(AmandaGreen);
        JohnNorton.getSubordinateList().add(AlexDillon);
        AshleySimpson.getSubordinateList().add(AndrewSampson);
        ChristineBennet.getSubordinateList().add(DavidGalloway);
        ChristineBennet.getSubordinateList().add(BradTurner);
        ChristineBennet.getSubordinateList().add(DavidChand);
        AmandaGreen.getSubordinateList().add(VictoriaBell);
        AmandaGreen.getSubordinateList().add(AndrewBlack);
        KateHudson.getSubordinateList().add(CharlotteHond);
        KateHudson.getSubordinateList().add(JoanneDowman);
        ScarletGreen.getSubordinateList().add(ChristineBennet);
        ScarletGreen.getSubordinateList().add(MichaelPerry);
        JuliaSmith.getSubordinateList().add(PeterGordon);
        MichaelBrown.getSubordinateList().add(HelenChan);
        VictoriaBell.getSubordinateList().add(JohnNorton);
        VictoriaBell.getSubordinateList().add(AndrewBlank);
        VictoriaBell.getSubordinateList().add(JamesCarol);
        CharlotteHond.getSubordinateList().add(AshleySimpson);
        CharlotteHond.getSubordinateList().add(CarolWhite);
        CharlotteHond.getSubordinateList().add(ElenaTang);
        CharlotteHond.getSubordinateList().add(TessaGreen);
        
        //declaring and initialising MainForm, with ArrayList of Person type as a parameter
        //then setting form to visible
        MainForm mainForm = new MainForm(listOfAllPersons);
        mainForm.setVisible(true);
        System.out.println("Main Form created");
    }
    
}
