package oca.project;
/**
 * Class used to define SalesConsultant objects 
 */
public class SalesConsultant extends ContractSubordinate {

    public SalesConsultant(IManager manager) {
        super(27.50, manager);
    }

}
