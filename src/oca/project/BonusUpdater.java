
package oca.project;


public class BonusUpdater {

    //class is used when manager wants to assign a bonus to a subordinate
    
    //static method to assign bonuses for other classes
    public static String assignBonus(IManager manager, ISubordinate subordinate, double bonus){
        //declaring variables: count checks if theres a match for subordinates,
        //salariedSubordinate is used to convert "subordinate" variable so bonuses can be assigned
        int count = 0;
        ISalariedPerson salariedSubordinate = null;
        String returnMessage = "";
        try{
            //enhanced for loop to go through all subordinates of the manager,
            //checking if there is a match
            for (ISubordinate s: manager.getSubordinateList()){
                if (s.equals(subordinate)){
                    count++;
                }
            }
            //if no match is found, throw exception
            if (count == 0){
                throw new Exception(); 
            }
            //if match is found, and bonus is not exceeded - cast subordinate to ISalariedPerson
            else if (count != 0){
                if (bonus > manager.getTotalManagerBonusFund()){
                    returnMessage = "Manager cannot allocate more than $" + manager.getTotalManagerBonusFund();
                }
                else {
                    salariedSubordinate = (ISalariedPerson) subordinate;
                }
            }
            //checking to see if bonus is already applied, if not then apply bonus
            if (salariedSubordinate.getCurrentBonus() != 0){
                returnMessage =  salariedSubordinate.toString() + " has already got a bonus assigned";
            }
            else {
                salariedSubordinate.setCurrentBonus(bonus);
                manager.applyBonus(bonus);
                returnMessage =  "The bonus has successfully been applied";
            }
        }
        catch (NullPointerException npe){
            npe.toString();
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        return returnMessage;
    }
}
