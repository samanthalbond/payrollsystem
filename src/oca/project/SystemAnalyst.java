package oca.project;
/**
 * Class used to define SystemsAnalyst objects 
 */
public class SystemAnalyst extends SalariedSubordinate {

    public SystemAnalyst(IManager manager) {
        super(75_000.00, manager);
    }

}
