package oca.project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
/**
 *
 * Form that allows the running of various reports 
 */
public class PayrollReportsForm extends javax.swing.JFrame {
    /**
     * Creates new form ReportForm
     * @param listOfReportItems
     */
    public PayrollReportsForm(ArrayList<PayrollReportItem> listOfReportItems) {
        initComponents(); 
        this.getContentPane().setBackground(new java.awt.Color(231,231,231));
    }

    //setting date format
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    //declaring ArrayList to store "Person" type objects
    private ArrayList<Person> listOfAllPersons;
    //encapsulating ArrayList of Person
    public ArrayList<Person> getListOfAllPersons(){
        return listOfAllPersons;
    }
    public void setListOfAllPersons(ArrayList<Person> listOfAllPersons) {
        this.listOfAllPersons = listOfAllPersons;
    }
    
    //declaring and encapulating MainForm which will be used for navigation between forms
    private MainForm mainForm ;
    
    public MainForm getMainForm() {
        return mainForm;
    }
    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }
    
    //creating new list to hold the report items
    private ArrayList<PayrollReportItem> listOfReportItems = new ArrayList<>();
    //encapsulating listOfReportItems
    public ArrayList<PayrollReportItem> getListOfReportItems() {
        return listOfReportItems;
    }
    public void setListOfReportItems(ArrayList<PayrollReportItem> listOfReportItems) {
        this.listOfReportItems = listOfReportItems;
    }
    
    //creating method to populate names of all Person objects in cboName
    public void populateNameComboBox(ArrayList<Person> listOfAllPersons){
        DefaultComboBoxModel nameModel = new DefaultComboBoxModel();
        for (Person p: listOfAllPersons){
            nameModel.addElement(p);
        }
        cboName.setModel(nameModel);
    }
    
    public void clear(){
        txtReport.setText("");
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        msgMessage = new javax.swing.JOptionPane();
        btnPaymentForPeriod = new javax.swing.JButton();
        btnBonusReport = new javax.swing.JButton();
        btnPaymentHistoryForPerson = new javax.swing.JButton();
        btnFortnighlyPayments = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnMonthlyPayments = new javax.swing.JButton();
        cboName = new javax.swing.JComboBox();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtFirstDate = new javax.swing.JTextField();
        txtSecondDate = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtReport = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Best IT - Payroll Reports");

        btnPaymentForPeriod.setBackground(new java.awt.Color(216, 244, 255));
        btnPaymentForPeriod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnPaymentForPeriod.setText("All payments for period");
        btnPaymentForPeriod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentForPeriodActionPerformed(evt);
            }
        });

        btnBonusReport.setBackground(new java.awt.Color(216, 244, 255));
        btnBonusReport.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBonusReport.setText("Bonuses report");
        btnBonusReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBonusReportActionPerformed(evt);
            }
        });

        btnPaymentHistoryForPerson.setBackground(new java.awt.Color(216, 244, 255));
        btnPaymentHistoryForPerson.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnPaymentHistoryForPerson.setText("Payment history by person");
        btnPaymentHistoryForPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentHistoryForPersonActionPerformed(evt);
            }
        });

        btnFortnighlyPayments.setBackground(new java.awt.Color(216, 244, 255));
        btnFortnighlyPayments.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnFortnighlyPayments.setText("List Fortnightly Payments");
        btnFortnighlyPayments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFortnighlyPaymentsActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Payroll Reports");

        btnMonthlyPayments.setBackground(new java.awt.Color(216, 244, 255));
        btnMonthlyPayments.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnMonthlyPayments.setText("List Monthly Payments");
        btnMonthlyPayments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMonthlyPaymentsActionPerformed(evt);
            }
        });

        cboName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cboName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnBack.setBackground(new java.awt.Color(216, 244, 255));
        btnBack.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("First Date:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Second Date:");

        txtFirstDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtSecondDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtReport.setBackground(new java.awt.Color(216, 216, 216));
        txtReport.setColumns(20);
        txtReport.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtReport.setRows(5);
        jScrollPane1.setViewportView(txtReport);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(809, 809, 809)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(26, 26, 26)
                                        .addComponent(txtFirstDate, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(8, 8, 8)
                                        .addComponent(txtSecondDate, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnPaymentForPeriod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnBonusReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnPaymentHistoryForPerson, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnFortnighlyPayments, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnMonthlyPayments, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSeparator3)
                                    .addComponent(cboName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSeparator1)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtFirstDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txtSecondDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(btnPaymentForPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(btnBonusReport, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(btnPaymentHistoryForPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnFortnighlyPayments, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnMonthlyPayments, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    

    //event handler for the Back button that closes the current form and displays the main form 
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.dispose();
        try {
            mainForm = new MainForm(getListOfAllPersons());
        } catch (ParseException pe) {
            pe.toString();
        }
        getMainForm().setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    //event handler for the button that displays all payments made within a period between two dates
    private void btnPaymentForPeriodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentForPeriodActionPerformed
        try {
            //clear();
            int count = 0;
            mainForm = new MainForm(getListOfAllPersons());
            //checks to make sure dates are entered in both textboxes
            //if dates are not entered, displays message to user
            if (txtFirstDate.getText().equals("") || txtSecondDate.getText().equals("")){
                msgMessage.showMessageDialog(this, "Please enter dates in both of the"
                        + " textboxes", "Annoucement", JOptionPane.INFORMATION_MESSAGE);
            }
            //if both textboxes contain dates, then loops through MainForm's listOfPayrollReportItems
            //and if an items date is between the two given dates, displays details to txtReport
            else {
                Date firstDate = sdf.parse(txtFirstDate.getText());
                Date secondDate = sdf.parse(txtSecondDate.getText());
                for (PayrollReportItem p: MainForm.getListOfPayrollReportItems()){
                    if (!firstDate.after(p.getStartOfPayPeriod()) && !secondDate.before(p.getStartOfPayPeriod())){
                        txtReport.setText(txtReport.getText() + p.toString() + "\r\n");
                        count++;
                    }
                }
                //checks if count variable is still set to one, if so displays message to the user
                if (count == 0){
                msgMessage.showMessageDialog(this, "There are no results to display", 
                        "Announcement", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } 
        catch (ParseException pe) {
            msgMessage.showMessageDialog(this, pe.toString(), "Parse Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPaymentForPeriodActionPerformed

    //event handler for the button that displays 
    //information about all the bonuses assigned to subordinates
    private void btnBonusReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBonusReportActionPerformed
        try{
            //clear();
            int count = 0;
            //loops through listOfAllPersons and checks to see if each Person object 
            //is an instance of SalariedSubordinate. If so, and if that object has been 
            //assigned a bonus, then displays details to txtReport
            for (Person p: listOfAllPersons){
                if (p instanceof SalariedSubordinate){
                    SalariedSubordinate salariedPerson = (SalariedSubordinate) p;
                    if (salariedPerson.getCurrentBonus() != 0.0){
                        txtReport.setText(txtReport.getText() + p.toString() + " has "
                                + "a bonus of $" + SalaryPaymentCalculator.df.format(salariedPerson.getCurrentBonus()) + "\n");
                        count++;
                    }
                }
            }
            //checks if count variable is still set to one, if so displays message to the user
            if(count == 0) {
                msgMessage.showMessageDialog(this, "There are no results to "
                        + "display", "Announcement", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnBonusReportActionPerformed

    //event handler for the button that displays all payments for the employee selected in the cboName
    private void btnPaymentHistoryForPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentHistoryForPersonActionPerformed
        try{
            Person selectedPerson = (Person) cboName.getSelectedItem();
            int count = 0;
            //creating seperate list of PayrollReportItems taking items from mainForm
            ArrayList<PayrollReportItem> listItems = MainForm.getListOfPayrollReportItems();
                //for each PayrollReportItem in listItems, checks if the Person selected in the textbox
                //matches a report item and if so increments the count variable
                for (PayrollReportItem item: listItems){ 
                    if (item.getPerson() == selectedPerson){
                       count++;
                    }
                } 
                //if count is greater than zero, then loops through MainForms list of PayrollReportItems
                //and when it reaches the match adds detail to txtReport
                if (count > 0){
                    for (PayrollReportItem pri: MainForm.getListOfPayrollReportItems()){ 
                        if (pri.getPerson() == selectedPerson){ //add clear method, then if txtReport empty, throw error message
                           txtReport.setText(txtReport.getText() + pri.toString() + "\r\n");
                        }  
                    }
                }
                //if no matches found then show error message to user
                else if (count == 0){
                   msgMessage.showMessageDialog(this, "There are no results to display", 
                   "Announcement", JOptionPane.INFORMATION_MESSAGE);
                }
        }
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPaymentHistoryForPersonActionPerformed

    //event handler for the button displaying all fortnightly payments made
    private void btnFortnighlyPaymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFortnighlyPaymentsActionPerformed
        try {
            //clear();
            int count = 0;
            //iterates through listOfPayroll items and if ArrayList contains items that
            //have a fortnightly pay date, sets their details to txtReport
                for (PayrollReportItem p: MainForm.getListOfPayrollReportItems()){
                    if (p.getTimePeriod() == TimePeriod.FORTNIGHTLY){  
                        txtReport.setText(txtReport.getText() + p.toString() + "\r\n");
                        count++;
                    }  
                }   
                //checks if count variable is still set to one, if so displays message to the user
                if (count == 0){
                        msgMessage.showMessageDialog(this, "There are no results to display", 
                                "Announcement", JOptionPane.INFORMATION_MESSAGE);
            }
        } 
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnFortnighlyPaymentsActionPerformed

    //event handler for the button displaying all monthly payments made
    private void btnMonthlyPaymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMonthlyPaymentsActionPerformed
        try {
            //clear();
            int count = 0;
            //if MainForm's listOfPayrollItems contains items, and those items are Monthly objects
            //then displays items details to txtReport
                for (PayrollReportItem p: MainForm.getListOfPayrollReportItems()){
                    if (p.getTimePeriod() == TimePeriod.MONTHLY){  
                        txtReport.setText(txtReport.getText() + p.toString() + "\r\n");
                        count++;
                    }  
                } 
                //checks if count variable is still set to one, if so displays message to the user
                if (count == 0){
                        msgMessage.showMessageDialog(this, "There are no results to display", 
                                "Announcement", JOptionPane.INFORMATION_MESSAGE);
            }
        } 
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Exception Caught", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnMonthlyPaymentsActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ReportForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ReportForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ReportForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ReportForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new ReportForm().setVisible(true);
//            }
//        });
//    }
    
    //populates cboName with all the perople working in the company

         

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnBonusReport;
    private javax.swing.JButton btnFortnighlyPayments;
    private javax.swing.JButton btnMonthlyPayments;
    private javax.swing.JButton btnPaymentForPeriod;
    private javax.swing.JButton btnPaymentHistoryForPerson;
    private javax.swing.JComboBox cboName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JOptionPane msgMessage;
    private javax.swing.JTextField txtFirstDate;
    private javax.swing.JTextArea txtReport;
    private javax.swing.JTextField txtSecondDate;
    // End of variables declaration//GEN-END:variables

    


}
