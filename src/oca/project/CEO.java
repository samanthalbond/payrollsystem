package oca.project;

import java.util.ArrayList;
import java.util.Date;
/**
 * Class used to define CEO objects 
 */
public class CEO extends Person implements IManager, ISalariedPerson {

    //declaring instance variables
    private TimePeriod salaryFrequency;
    private double currentBonus;
    private final double BASE_SALARY = 110000;
    private double managerBonusFund = 5000;
    private Date startDate;
    private static ArrayList<ISubordinate> subordinateList = new ArrayList<>();

    //getter and setter methods for instance variables
    @Override
    public ArrayList<ISubordinate> getSubordinateList() {
        return subordinateList;
    }

    @Override
    public double getTotalManagerBonusFund() {
        return managerBonusFund;
    }

    @Override
    public void setTotalManagerBonusFund(double totalManagerBonusFund) {
        this.managerBonusFund = managerBonusFund;
    }

    @Override
    public void applyBonus(double bonus) {
        managerBonusFund = (managerBonusFund - bonus);
    }

    @Override
    public String assignBonus(ISubordinate subordinate, double bonus) {
        return BonusUpdater.assignBonus(this, subordinate, bonus);
    }

    @Override
    public TimePeriod getTimePeriod() {
        return salaryFrequency;
    }

    @Override
    public void setTimePeriod(TimePeriod timePeriod) {
        this.salaryFrequency = timePeriod;
    }
    
    @Override
    public double getBaseSalary() {
        return BASE_SALARY;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }
    
    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    @Override
    public double getCurrentBonus() {
        return currentBonus;
    }

    @Override
    public void setCurrentBonus(double bonus) {
        this.currentBonus = bonus;
    }

    @Override
    public double calculatePay() throws Exception {
        return SalaryPaymentCalculator.calculatePay(this);
    }

    

}
