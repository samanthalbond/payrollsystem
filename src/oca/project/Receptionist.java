package oca.project;
/**
 * Class used to define Receptionist objects 
 */
public class Receptionist extends SalariedSubordinate {

    public Receptionist(IManager manager) {
        super(35_000.00, manager);
    }

}
