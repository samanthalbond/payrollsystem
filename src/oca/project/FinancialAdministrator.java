package oca.project;
/**
 * Class used to define FinancialAdminstrator objects 
 */
public class FinancialAdministrator extends SalariedSubordinate {

    public FinancialAdministrator(IManager manager) {
        super(75_000.00, manager);
    }

}
