package oca.project;
/**
 * Class used to define SalesManager objects 
 */
public class SalesManager extends Manager {

    public SalesManager(IManager manager) {
        super(75_000.00, manager);
    }

}
