package oca.project;

import java.util.Date;
/**
 * Class used to define ContractSubordinate objects 
 */
public class ContractSubordinate extends Person implements IContractor {

    //declaring instance variables
    private IManager manager;
    private double hourlyRate;
    
    //declaring constructor
    public ContractSubordinate(double hourlyRate, IManager manager){
        this.hourlyRate = hourlyRate;
        this.manager = manager;
    }
    
    //method to calculate and return totalPay for certain period for a subordinate
    @Override
    public double calculatePay(Date periodStartDate, double numberOfHours) throws Exception {
        double totalPay = 0.0;
        double overTimeRate = (hourlyRate * 0.1) + hourlyRate;
        if (numberOfHours > 160){
            for (int i = 1; i <= 160; i++){
                totalPay += hourlyRate;
            }
            for (int j = 160; j <= numberOfHours; j++){
                totalPay += overTimeRate;
            }
        }
        else if (numberOfHours <= 160){
            for (int i = 1; i <= numberOfHours; i++){
                totalPay += hourlyRate;
            }
        }
        return totalPay;
    }

    //getter and setter methods for instance variables
    @Override
    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate){
        this.hourlyRate = hourlyRate;
    }
    
    @Override
    public IManager getManager() {
        return manager;
    }

}
