package oca.project;
/**
 * Class used to define ProgramDeveloper objects 
 */
public class ProgramDeveloper extends SalariedSubordinate {

    public ProgramDeveloper(IManager manager) {
        super(70_000.00, manager);
    }

}
