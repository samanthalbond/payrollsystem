package oca.project;

import java.util.Date;

public class PayrollReportItem {
    
    //class to hold information about payments being made
    
    //declaring instance variables
    private Person person;
    private double salary;
    private Date startOfPayPeriod;
    private TimePeriod payPeriod;
    
    //declaring constructor
    public PayrollReportItem(Person person, double salary, Date startOfPayPeriod, TimePeriod payPeriod){
        this.person = person;
        this.salary = salary;
        this.startOfPayPeriod = startOfPayPeriod;
        this.payPeriod = payPeriod;
    }
    
    //toString method to return formatted message for use on Payroll Reports Form
    public String toString(){
        return person.toString() + " was paid "
                + SalaryPaymentCalculator.df.format(salary) + " on the " + startOfPayPeriod + ". Payment Period: " + payPeriod;
    }
    
    //getter methods for the class
    public Date getStartOfPayPeriod(){
        return startOfPayPeriod;
    }
    
    public Person getPerson(){
        return person;
    }
    
    public TimePeriod getTimePeriod(){
        return payPeriod;
    }
}
