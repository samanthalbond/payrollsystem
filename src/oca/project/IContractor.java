package oca.project;

import java.util.Date;

//Interface that all contractors should implement
public interface IContractor extends ISubordinate{
    
    //this method should be used by all contractors
    //to calculate the pay earned over a period of time
    public double calculatePay(Date periodStartDate, double numberOfHours) throws Exception ;
    
    //getter method for find hourly rate, should be used by all contractors
    public double getHourlyRate();
}
