package oca.project;
/**
 * Class used to define AdministrationManager objects 
 */
public class AdministrationManager extends Manager{
    
    public AdministrationManager(IManager manager) {
        super(100_000.00, manager);
    }

}
